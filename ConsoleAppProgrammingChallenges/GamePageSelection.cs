using ConsoleAppProgrammingChallenges.challenges;
using ConsoleAppProgrammingChallenges.challenges._05_FizzBuzz;
using ConsoleAppProgrammingChallenges.challenges._06_RockPaperScissors;
using ConsoleAppProgrammingChallenges.challenges._07_ProjectEuler;
using ConsoleAppProgrammingChallenges.challenges._08_Hangman;
using ConsoleAppProgrammingChallenges.challenges._09_LoveCalculator;
using ConsoleAppProgrammingChallenges.challenges._10_RandomSentenceGenerator;
using ConsoleAppProgrammingChallenges.challenges._11_RandomPasswordGenerator;
using ConsoleAppProgrammingChallenges.utils;

namespace ConsoleAppProgrammingChallenges;

public class GamePageSelection
{
    private readonly List<IChallenge> _games = new()
    {
        new FindTheCorrectValueChallenge(),
        new TemperatureConverterChallenge(),
        new CalculateAgeInSecondsChallenge(),
        new EncryptionDecryptionAlgorithmChallenge(),
        new FizzBuzzChallenge(),
        new RockPaperScissorsChallenge(),
        new ProjectEuler(),
        new Hangman(),
        new LoveCalculator(),
        new RandomSentenceGenerator(),
        new RandomPasswordGenerator(),
    };

    private string description = String.Join(
        "",
        Environment.NewLine,
        "Mini-game list:"
    );

    public void Start()
    {
        bool wantToRunAgain;
        do
        {
            wantToRunAgain = Run();
        } while (wantToRunAgain);
        
        Environment.Exit(0);
    }

    private bool Run()
    {
        Console.WriteLine(description);

        ShowMinigameList();
        Console.WriteLine();

        var minigameNumber = GetMinigameNumber();

        if (minigameNumber == -1) return false;

        var gameSelected = _games[minigameNumber];
        Console.WriteLine($"{Environment.NewLine}Selected game: {gameSelected.GetName()}");
        Console.WriteLine("Starting game...");

        gameSelected.Play();
        return true;
    }

    private void ShowMinigameList()
    {
        Console.WriteLine("----------");
        for (int i = 0; i < _games.Count; i++)
        {
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.Write($"{i + 1}- ");
            Console.ResetColor();
            Console.WriteLine(_games[i].GetName());
        }
        Console.WriteLine("----------");
    }

    private int GetMinigameNumber()
    {
        int minigameNumber;
        do
        {
            minigameNumber = ConsoleUtils.TryRepeatedlyUntilGetANumberFromUser(
                "Type the mini-game number you want do play(or type -1 if you want do exit): "
            );

            if (minigameNumber == -1) return -1;

            if (minigameNumber > _games.Count || minigameNumber <= 0)
            {
                ConsoleUtils.WriteLineErrorMessage("There's no minigame with this number. Try again");
            }

            
        } while (minigameNumber <= 0 || minigameNumber > _games.Count);

        return minigameNumber - 1;
    }
}