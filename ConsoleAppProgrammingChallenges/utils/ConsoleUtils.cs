using System.Text;

namespace ConsoleAppProgrammingChallenges.utils;

public static class ConsoleUtils
{
    /// <summary> try repeatedly to get a number of a user until the user type a number </summary>
    /// <returns> int32 value  </returns>
    public static int TryRepeatedlyUntilGetANumberFromUser(string description)
    {
        var number = 0;
        bool gotExceptionOnANumber = false;
        do
        {
            if (gotExceptionOnANumber)
            {
                WriteLineErrorMessage("Please, type a number");
            }
            Console.Write(description);
            try
            {
                var numberGot = int.Parse(Console.ReadLine() ?? string.Empty);
                number = numberGot;
                gotExceptionOnANumber = false;
            }
            catch (Exception e) when (e is FormatException or OverflowException)
            {
                gotExceptionOnANumber = true;
            }
        } while (gotExceptionOnANumber);

        return number;
    }

    public static int TryRepeatedlyUntilGetANonNegativeNumberFromUser(string description)
    {
        var number = 0;
        bool gotExceptionOnANumber = false;
        do
        {
            if (gotExceptionOnANumber)
            {
                WriteLineErrorMessage("Please, type a non negative number");
            }
            Console.Write(description);
            try
            {
                var numberGot = int.Parse(Console.ReadLine() ?? string.Empty);
                number = numberGot;
                gotExceptionOnANumber = number < 0;
            }
            catch (Exception e) when (e is FormatException or OverflowException)
            {
                gotExceptionOnANumber = true;
            }
        } while (gotExceptionOnANumber);

        return number;
    }
    public static int TryRepeatedlyUntilGetMinus1OrProvidedListNumbersFromUser(string description, int[] numList)
    {
        var number = 0;
        bool notValidNumber = false;
        do
        {
            if (notValidNumber)
            {
                WriteLineErrorMessage("Please, type a correct number or -1");
            }
            Console.Write(description);
            try
            {
                var numberGot = int.Parse(Console.ReadLine() ?? string.Empty);
                if (!numList.Contains(numberGot) && numberGot != -1)
                {
                    notValidNumber = true;
                    continue;
                }

                number = numberGot;
                notValidNumber = false;
            }
            catch (Exception e) when (e is FormatException or OverflowException)
            {
                notValidNumber = true;
            }
        } while (notValidNumber);

        return number;
    }
    
    public static void WriteLineErrorMessage(string message)
    {
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine(message);
        Console.ResetColor();
    }

    public static void WriteNumeratedList(List<string> list, string? title = null)
    {
        if (title != null) Console.WriteLine(title);
        Console.WriteLine("----------");
        for (int i = 0; i < list.Count; i++)
        {
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.Write($"{i + 1}- ");
            Console.ResetColor();
            Console.WriteLine(list[i]);
        }
        Console.WriteLine("----------");
    }

    public static void PrintIntList(List<int> list) {

        var listStringBuilder = new StringBuilder("[ ");

        list.ForEach(num => listStringBuilder.Append($"{num.ToString()}, "));
        var listFormatted = listStringBuilder.Remove(listStringBuilder.Length - 2, 1).Append(']');

        Console.WriteLine(listFormatted);
    }
}