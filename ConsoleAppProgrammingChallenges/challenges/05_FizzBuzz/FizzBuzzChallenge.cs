using System.Text;
using ConsoleAppProgrammingChallenges.utils;

namespace ConsoleAppProgrammingChallenges.challenges._05_FizzBuzz;

public class FizzBuzzChallenge : IChallenge
{
    public string GetName()
    {
        return "Fizz Buzz";
    }

    public void Play()
    {
        var quantity = -2;
        do
        {
            quantity =
                ConsoleUtils.TryRepeatedlyUntilGetANumberFromUser("How many numbers do you wanna test:");

            if (quantity < -1)
            {
                ConsoleUtils.WriteLineErrorMessage("Please, type -1 to quit or a non-negative number");
            }
        } while (quantity < -1);

        if (quantity == -1)
        {
            (this as IChallenge).ExitChallengeText();
            return;
        }

        var fizzBuzzList = GenerateFizzBuzzList(quantity);

        var fizzBuzzStringFormatted = "[ ]";

        if (fizzBuzzList.Count > 0)
        {
            var fizzBuzzStringBuilder = new StringBuilder("[ ");
            foreach (string value in fizzBuzzList)
            {
                fizzBuzzStringBuilder.Append($"{value}, ");
            }

            fizzBuzzStringFormatted =
                fizzBuzzStringBuilder.Remove(fizzBuzzStringBuilder.Length - 2, 1).Append(']').ToString();
        }


        Console.WriteLine("FizzBuzz list generated:");
        Console.WriteLine(fizzBuzzStringFormatted);

        (this as IChallenge).ExitChallengeText();
    }

    private List<String> GenerateFizzBuzzList(int quantity)
    {
        if (quantity == 0) return new List<string>();

        var fizzBuzzList = new List<string>();

        for (int i = 1; i <= quantity; i++)
        {
            bool divibisleBy3 = false;
            bool divisibleBy5 = false;
            var result = "";
            if (i % 3 == 0)
            {
                result += "Fizz";
                divibisleBy3 = true;
            }

            if (i % 5 == 0)
            {
                result += "Buzz";
                divisibleBy5 = true;
            }

            if (!divibisleBy3 && !divisibleBy5)
            {
                result += i.ToString();
            }

            fizzBuzzList.Add(result);
        }

        return fizzBuzzList;
    }
}