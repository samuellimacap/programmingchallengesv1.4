using ConsoleAppProgrammingChallenges.utils;

namespace ConsoleAppProgrammingChallenges.challenges;

public class FindTheCorrectValueChallenge : IChallenge
{
    private string gameDescription = String.Join(
        "",
        Environment.NewLine,
        "Description: Your objective is to find the correct number.",
        Environment.NewLine,
        Environment.NewLine,
        "To start the game, select the difficulty level",
        Environment.NewLine,
        "   After that, type a random number, between the range selected, and press Enter. If you guessed right, the game " +
        "will answer 'You guessed right' and will ends.",
        Environment.NewLine,
        "   If you guessed wrong, the game will answer if the correct number is HIGHER or LOWER the number you typed. " +
        "You have unlimited attempts to guess the correct number.",
        Environment.NewLine,
        Environment.NewLine,
        "If you want to quit the game, type -1 and press Enter."
    );

    private Dictionary<String, int> rangeByDifficultMap =
        new() { { "Easy", 10 }, { "medium", 50 }, { "hard", 100 }, { "ultra-hard", 1000 } };

    public void Play()
    {
        (this as IChallenge).ShowNameTemplate();

        Console.WriteLine(gameDescription);
        ShowDifficultLevels();

        var selectedLevel = SelectDifficultLevel();
        if (selectedLevel == null) return;

        StartGame(selectedLevel.Value.Value);

        (this as IChallenge).ExitChallengeText();
    }

    private void ShowDifficultLevels()
    {
        Console.WriteLine("Difficulty level:\n");
        Console.WriteLine("---");
        var i = 0;
        foreach (var keyValuePair in rangeByDifficultMap)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write($"{i + 1}- ");
            Console.ResetColor();
            Console.WriteLine($"{keyValuePair.Key} (0-{keyValuePair.Value})");
            i++;
        }

        Console.WriteLine("---");
    }

    private KeyValuePair<string, int>? SelectDifficultLevel()
    {
        int typedNumber;
        bool userTypedRight;
        do
        {
            userTypedRight = true;

            typedNumber =
                ConsoleUtils.TryRepeatedlyUntilGetANumberFromUser("Select difficulty(or type -1 to go back): ");
            if (typedNumber == -1) return null;
            if (typedNumber is 0 or < -1)
            {
                userTypedRight = false;
                ConsoleUtils.WriteLineErrorMessage("Please select one of theses numbers shown above");
            }

            if (typedNumber > rangeByDifficultMap.Count)
            {
                userTypedRight = false;
                ConsoleUtils.WriteLineErrorMessage("Difficulty doesn't exist");
            }
        } while (!userTypedRight);

        var selectedLevel = rangeByDifficultMap.ElementAt(typedNumber - 1);
        Console.WriteLine($"{Environment.NewLine}Selected difficulty: {selectedLevel.Key}");
        return selectedLevel;
    }

    private void StartGame(int limit)
    {
        var numberToGuess = new Random().Next(limit);
        var isGuessedRight = false;
        int attempts = 0;
        while (!isGuessedRight)
        {
            attempts++;
            var number = TryToGetANumberFromUser();

            if (number == -1) return;
            if (number == numberToGuess)
            {
                isGuessedRight = true;
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("You guessed right");
                Console.ResetColor();
            }
            else
                Console.WriteLine(
                    number > numberToGuess
                        ? "Lower"
                        : "Higher");
        }

        Console.WriteLine("---");
        Console.WriteLine($"Total attempts: {attempts}");
        Console.WriteLine("---");
    }
    

    private int TryToGetANumberFromUser()
    {
        var number = 0;
        bool gotExceptionOnANumber = false;
        do
        {
            if (gotExceptionOnANumber) Console.WriteLine("Please, type a number or type -1 if you want do exit");
            Console.Write("Enter a number: ");
            try
            {
                var numberGot = int.Parse(Console.ReadLine());
                number = numberGot;
                gotExceptionOnANumber = false;
            }
            catch (Exception e) when (e is FormatException or OverflowException)
            {
                gotExceptionOnANumber = true;
            }
        } while (gotExceptionOnANumber);

        return number;
    }

    public string GetName()
    {
        return "Find the correct value";
    }
}