namespace ConsoleAppProgrammingChallenges.challenges._07_ProjectEuler;

public class CalcSumSquareDifference
{
    private int _length {get; set;}

    public CalcSumSquareDifference(int length) {
        _length = length;
    }

    public void CalculateAndPrint() {
        var sumOftheSquares = 0; 
        for (int i = 1; i <= _length; i++) {
            sumOftheSquares += (int) Math.Pow(i,2);
        }

        var squareSumOfTheNumbers = 0;
        for (int i = 1; i <= _length; i++) {
            squareSumOfTheNumbers += i;
        }

        squareSumOfTheNumbers = (int) Math.Pow(squareSumOfTheNumbers,2);

        var sumSquareDifference =  squareSumOfTheNumbers - sumOftheSquares;

        Console.WriteLine($"1² + 2² + ... + {_length}² = {sumOftheSquares}");
        Console.WriteLine("----------");
        Console.WriteLine($"(1 + 2 + ... + {_length})² = {squareSumOfTheNumbers}");
        Console.WriteLine("----------");
        Console.WriteLine($"{squareSumOfTheNumbers} - {sumOftheSquares} = {sumSquareDifference}");
    }
}