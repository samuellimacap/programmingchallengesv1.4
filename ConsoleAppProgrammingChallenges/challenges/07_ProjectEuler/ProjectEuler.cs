using System.Numerics;
using System.Text;
using ConsoleAppProgrammingChallenges.utils;

namespace ConsoleAppProgrammingChallenges.challenges._07_ProjectEuler;

public class ProjectEuler : IChallenge
{
    private Dictionary<int, string> problemsList = new Dictionary<int, string>()
    {
        { 1, "Multiple of 3 or 5 below 1000" },
        { 2, "Sum of even-valued terms in a list of 4000 fibonacci terms" },
        { 3, "Largest prime factor of 6009514" },
        { 4, "Largest palindrome product"},
        { 5, "Smallest multiple by all numbers from 1 to 20"},
        { 6, "Sum square difference of 100 natural numbers"},
        { 7, "10 001th prime number"}
    };

    public string GetName()
    {
        return "Project Euler";
    }

    public void Play()
    {
        ConsoleUtils.WriteNumeratedList(problemsList.Values.ToList(), "Project euler problems");

        var numberChose =
            ConsoleUtils.TryRepeatedlyUntilGetMinus1OrProvidedListNumbersFromUser("Select:",
                problemsList.Keys.ToArray());

        if (numberChose == 1)
        {
            var multipleOf3Or5Below1000 = MultipleOf3Or5Below1000();
            var sum = multipleOf3Or5Below1000.Sum();

            Console.WriteLine($"Sum of all multiple of 3 or 5 below 1000: {sum}");
        }
        else if (numberChose == 2)
        {
            var sum = SumOfEvenValuedFibonacciTerms(4000);

            Console.WriteLine($"Sum of even-valued terms: {sum}");
        }
        else if (numberChose == 3)
        {
            var value = 600_851_475_143;
            var primeArray = LargestPrimeFactor(value);

            PrintPrimeArrayFormatted(primeArray);
        }
        else if (numberChose == 4)
        {
            var CalcLargestPalindromeProduct = new CalcLargestPalindromeProduct();
            CalcLargestPalindromeProduct.CalculateAndPrint();
        } else if (numberChose == 5)
        {
            var CalcSmallestMultiple = new CalcSmallestMultiple();
            CalcSmallestMultiple.CalculateAndPrint();
        } else if (numberChose == 6)
        {
            var CalcSumSquareDifference = new CalcSumSquareDifference(100);
            CalcSumSquareDifference.CalculateAndPrint();
        } else if (numberChose == 7) 
        {
            var PrimeGenerator = new PrimeGenerator();
            var primeNumbers = PrimeGenerator.GeneratePrimes(10001);
            var primeNumber10001th =  primeNumbers[primeNumbers.Count - 1];
            Console.WriteLine($"10 001th prime number: {primeNumber10001th}");
        }
    }

    private List<int> MultipleOf3Or5Below1000()
    {
        var multipleOf3Or5List = new List<int>();
        for (int i = 1; i <= 1000; i++)
        {
            if (i % 3 == 0 || i % 5 == 0)
                multipleOf3Or5List.Add(i);
        }

        return multipleOf3Or5List;
    }

    private BigInteger SumOfEvenValuedFibonacciTerms(int termsLength)
    {
        var fibonacciList = new List<BigInteger>() { 1, 2 };

        for (int i = 1; i <= (termsLength - 2); i++)
        {
            fibonacciList.Add((fibonacciList[fibonacciList.Count - 1] + fibonacciList[fibonacciList.Count - 2]));
        }

        return fibonacciList.Where(i => i % 2 == 0).Aggregate(BigInteger.Add);
    }

    private long[] LargestPrimeFactor(long value)
    {
        var primeList = new List<long>();

        int count = -1;
        long pseudoValue = 0;
        do
        {
            count++;
            var primeNumber = PrimeGenerator.Primes().ElementAt(count);
            if (value % primeNumber != 0)
                continue;

            primeList.Add(primeNumber);

            Console.WriteLine($"Prime found: {primeNumber}");
            pseudoValue = primeList.Aggregate((a, x) => a * x);
        } while (pseudoValue < value);

        if (pseudoValue == value) return primeList.ToArray();
        else throw new Exception("the value is not a valid multiply of prime factors");
    }

    private void PrintPrimeArrayFormatted(long[] primeArray)
    {
        var primeArrayFormatted = "[ ]";
        if (primeArray.Length > 0)
        {
            var primeArrayStringBuilder = new StringBuilder("[ ");
            foreach (long actualValue in primeArray)
            {
                primeArrayStringBuilder.Append($"{actualValue}, ");
            }

            primeArrayFormatted =
                primeArrayStringBuilder.Remove(primeArrayStringBuilder.Length - 2, 1).Append(']').ToString();
        }

        Console.WriteLine($"{primeArrayFormatted}, largest prime factor is {primeArray[primeArray.Length - 1]}");
    }
}