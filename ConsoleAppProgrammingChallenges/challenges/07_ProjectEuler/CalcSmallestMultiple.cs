namespace ConsoleAppProgrammingChallenges.challenges._07_ProjectEuler;

public class CalcSmallestMultiple
{
  public void CalculateAndPrint() {
    var value = 0;
    for (int i = 1; ; i++)
    {
        var isDivisibleBy1To20 = true;
        //If the number its divisible from 11 to 20, it will be divisible from 1 to 10
        //Because all the number between 11 and 20 is divisible to one or more numbers from 1 to 10. 
        //And every number between 1 and 10, if mutiplied by 2, can be any number between 11 and 20.
        for (int d = 11; d <=20; d++)
        {
            if (i%d!=0) 
            {
                isDivisibleBy1To20 = false;
                break;
            }
        }
        if (isDivisibleBy1To20) {
            value = i;
            break;
        }
    }
    Console.WriteLine($"Smallest multiple evenly divislbe by 1 to 20: {value}");
  }
}