namespace ConsoleAppProgrammingChallenges.challenges._07_ProjectEuler;

public class PrimeGenerator
{
    public static bool IsPrime(int num)
    {
        if (num <= 1) return false;
        if (num == 2) return true;
        
        for (int i = 2; i * i <= num; i++)
            if (num % i == 0)
                return false;
        return true;
    }

    public static List<int> GeneratePrimesUntilNum(int n)
    {
        List<int> primes = new List<int>();
        
        for (int i = 2; i<=n; i++)
            if (IsPrime(i)) primes.Add(i);

        return primes;
    }


    public static IEnumerable<int> Primes()
    {
        for (int i = 2; ; i++)
            if (IsPrime(i))
                yield return i;
        
    }

    public static List<int> GeneratePrimes(int quantity)
    {
        List<int> primes = new List<int>();
        
        int i = 2;
        while (primes.Count <= quantity) {
            if (IsPrime(i)) primes.Add(i);
            i++;
        }
            

        return primes;
    }
}