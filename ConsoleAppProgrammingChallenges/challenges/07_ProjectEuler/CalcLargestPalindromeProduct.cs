
using ConsoleAppProgrammingChallenges.utils;

namespace ConsoleAppProgrammingChallenges.challenges._07_ProjectEuler;

public class CalcLargestPalindromeProduct
{
    public void CalculateAndPrint()
    {
        var max1 = 999;
        var max2 = 999;
        var higherPalindrome = 0;
        for (int i = 999; i >= 100; i--)
        {
            for (int j = 999; j >= 100; j--)
            {
                var value = i * j;
                if (isPalindrome(value))
                {
                    higherPalindrome = value > higherPalindrome ? value : higherPalindrome;
                }
            }
        }

        Console.WriteLine($"Largest Palindrome: {higherPalindrome}");
    }

    private bool isPalindrome(int value)
    {
        var valueString = value.ToString();
        var valueFirstHalf = valueString.Substring(0, (int)valueString.Length / 2);
        var valueSecondHalf = valueString.Substring((int)valueString.Length / 2).ToCharArray();
        Array.Reverse(valueSecondHalf);
        var valueSecondHalfReversed = new string(valueSecondHalf);


        if (valueFirstHalf.Equals(valueSecondHalfReversed)) 
            return true;

        return false;
    }
}