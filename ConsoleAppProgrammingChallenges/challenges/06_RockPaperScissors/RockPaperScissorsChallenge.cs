using ConsoleAppProgrammingChallenges.utils;
using Microsoft.Win32;

namespace ConsoleAppProgrammingChallenges.challenges._06_RockPaperScissors;

public class RockPaperScissorsChallenge : IChallenge
{
    private Dictionary<Option, JokenpoResult> rockAgainstMap = new()
    {
        { Option.Rock, new JokenpoResult(ConsoleColor.Yellow, "Draw") },
        { Option.Paper, new JokenpoResult(ConsoleColor.Red, "You loose") },
        { Option.Scissors, new JokenpoResult(ConsoleColor.Green, "You win") }
    };

    private readonly Dictionary<Option, JokenpoResult> paperAgainstMap = new()
    {
        { Option.Paper, new JokenpoResult(ConsoleColor.Yellow, "Draw") },
        { Option.Scissors, new JokenpoResult(ConsoleColor.Red, "You loose") },
        { Option.Rock, new JokenpoResult(ConsoleColor.Green, "You win") }
    };

    private readonly Dictionary<Option, JokenpoResult> scissorsAgainstMap = new()
    {
        { Option.Scissors, new JokenpoResult(ConsoleColor.Yellow, "Draw") },
        { Option.Rock, new JokenpoResult(ConsoleColor.Red, "You loose") },
        { Option.Paper, new JokenpoResult(ConsoleColor.Green, "You win") }
    };

    public string GetName()
    {
        return "Rock, paper, scissors";
    }

    public void Play()
    {
        bool repeat = false;
        do
        {
            bool askToRepeat = Start();
            if (askToRepeat)
            {
                repeat = (this as IChallenge).GetIfUserWantsToRestart();
            }
            else
            {
                repeat = false;
            }
        } while (repeat);
        
        (this as IChallenge).ExitChallengeText();
    }

    private bool Start()
    {
        var options = new Dictionary<int, string>() { { 1, "Rock" }, { 2, "Paper" }, { 3, "Scissors" } };
        ConsoleUtils.WriteNumeratedList(options.Values.ToList(), "Choose one: ");


        var optionChosen =
            ConsoleUtils.TryRepeatedlyUntilGetMinus1OrProvidedListNumbersFromUser(
                "Number: ",
                options.Keys.ToArray()
            );

        if (optionChosen == -1)
        {
            return false;
        }

        var enumChosen = (Option)optionChosen;

        Console.ForegroundColor = ConsoleColor.DarkGreen;
        Console.WriteLine("-->You: " + enumChosen.ToString());
        Console.ResetColor();

        Thread.Sleep(300);

        var randomInt = new Random(DateTime.Now.Microsecond).Next(1, 4);

        var enumChosenAgainst = (Option)randomInt;

        Console.WriteLine("----| Choosing randomly |----");
        Thread.Sleep(300);
        Console.ForegroundColor = ConsoleColor.Blue;
        Console.WriteLine("<-- Enemy: " + enumChosenAgainst.ToString());
        Console.ResetColor();

        Thread.Sleep(300);

        JokenpoResult result;
        switch (enumChosen)
        {
            case Option.Rock:
            {
                result = rockAgainstMap[enumChosenAgainst];
                break;
            }
            case Option.Paper:
            {
                result = paperAgainstMap[enumChosenAgainst];
                break;
            }
            case Option.Scissors:
            {
                result = scissorsAgainstMap[enumChosenAgainst];
                break;
            }
            default:
            {
                result = new JokenpoResult(ConsoleColor.White, "undefined");
                break;
            }
        }

        Console.Write("Result: ");
        Console.ForegroundColor = result.Color;
        Console.WriteLine(result.Message);
        Console.ResetColor();

        Thread.Sleep(300);
        return true;
    }
}

enum Option
{
    Rock = 1,
    Paper = 2,
    Scissors = 3,
}

record JokenpoResult(ConsoleColor Color, string Message);