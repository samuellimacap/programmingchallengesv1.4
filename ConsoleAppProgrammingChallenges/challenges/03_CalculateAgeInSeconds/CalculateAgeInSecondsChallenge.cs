using System.Diagnostics;
using System.Globalization;
using System.Text.RegularExpressions;
using ConsoleAppProgrammingChallenges.utils;

namespace ConsoleAppProgrammingChallenges.challenges;

public class CalculateAgeInSecondsChallenge : IChallenge
{
    public string GetName()
    {
        return "Calculate age in seconds";
    }

    public void Play()
    {

        bool toRepeat;
        do
        {
            (this as IChallenge).ShowNameTemplate();

            var birthDateTime = GetBirthDateFromUser();

            if (birthDateTime == null)
            {
                (this as IChallenge).ExitChallengeText();
                return;
            }

            var dtNow = DateTime.Now;

            var timesSpanNowMinusBirthDate = dtNow.Subtract((DateTime)birthDateTime);

            var ageInYears = double.Floor(timesSpanNowMinusBirthDate.TotalDays / 365);
            Console.WriteLine($"Age: {ageInYears}");

            var ageInSeconds = double.Floor(timesSpanNowMinusBirthDate.TotalSeconds);
            var ageInSecondsFormatted = String.Format(new NumberFormatInfo
            {
                NumberDecimalSeparator = ",",
                NumberGroupSeparator = "."
            }, "{0:N}", ageInSeconds);
            Console.WriteLine($"Age in seconds: {ageInSecondsFormatted} sec{(ageInSeconds > 1 ? "s" : "")}");

            toRepeat = (this as IChallenge).GetIfUserWantsToRestart();

        } while (toRepeat);
        
        (this as IChallenge).ExitChallengeText();
    }

    private DateTime? GetBirthDateFromUser()
    {
        bool userTypedRight = true;
        var regexDate = new Regex(@"(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/\d{4}");
        do
        {
            userTypedRight = true;
            Console.Write("Type your birthdate in dd/mm/yyyy format: ");
            var birthDateUser = Console.ReadLine()!.Trim();
            if (birthDateUser == "-1") return null;
            
            var matchRegexDate = regexDate.Match(birthDateUser);
            if (!matchRegexDate.Success)
            {
                ConsoleUtils.WriteLineErrorMessage("Your date is not match with the pattern 'dd/mm/yyyy'");
                ConsoleUtils.WriteLineErrorMessage($"Date sent by you: {birthDateUser}");
                userTypedRight = false;
                continue;
            }

            var dayMonthYearArray = birthDateUser.Split('/').Select(int.Parse).ToArray();
            var year = dayMonthYearArray[2];
            var month = dayMonthYearArray[1];
            var day = dayMonthYearArray[0];

            if (month == 2)
            {
                switch (day)
                {
                    case > 29:
                        ConsoleUtils.WriteLineErrorMessage($"February cannot have more than 29 days");
                        userTypedRight = false;
                        continue;
                    case 29 when !IsYearALeapYear(year):
                        ConsoleUtils.WriteLineErrorMessage($"{year} isn't a leap year. Therefore february cannot have 29 days");
                        userTypedRight = false;
                        continue;
                }
            }

            var dtBirthDate = new DateTime(dayMonthYearArray[2], dayMonthYearArray[1], dayMonthYearArray[0]);
            return dtBirthDate;

        } while (!userTypedRight);

        return null;
    }

    private bool IsYearALeapYear(int year)
    {
        if (year%4 != 0) return false;
        if (year % 100 == 0 && year % 400 != 0) return false;
        return true;
    }
}