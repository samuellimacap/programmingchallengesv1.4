namespace ConsoleAppProgrammingChallenges.challenges;

public interface IChallenge
{
    string GetName();

    void Play();


    void ShowNameTemplate()
    {
        Console.ForegroundColor = ConsoleColor.Blue;
        Console.WriteLine("--------------------");
        Console.WriteLine($"{GetName()}");
        Console.WriteLine("--------------------");
        Console.ResetColor();
    }

    void ExitChallengeText()
    {
        Console.ForegroundColor = ConsoleColor.DarkMagenta;
        Console.WriteLine("Closing challenge");
        Console.ResetColor();
    }

    bool GetIfUserWantsToRestart()
    {
        Console.WriteLine($"{Environment.NewLine}---");
        Console.WriteLine("Want to try again: ");
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine("Y - yes");
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("N or any other character - no");
        Console.ResetColor();

        Console.Write(Environment.NewLine + "Type: ");
        var option = Console.ReadLine()!.Trim().ToUpper();

        return option == "Y";
    }
}