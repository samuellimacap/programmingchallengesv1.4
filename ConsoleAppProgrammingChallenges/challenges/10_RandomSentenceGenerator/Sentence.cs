namespace ConsoleAppProgrammingChallenges.challenges._10_RandomSentenceGenerator;

public class Sentence
{
    public required string sentence { get; set; }

    public Sentence(string sentence)
    {
        this.sentence = sentence;
    }
}