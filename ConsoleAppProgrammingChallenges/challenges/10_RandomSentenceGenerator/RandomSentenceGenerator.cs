using System.Net.Sockets;
using System.Text.Json;
using System.Text.Json.Nodes;
using ConsoleAppProgrammingChallenges.utils;

namespace ConsoleAppProgrammingChallenges.challenges._10_RandomSentenceGenerator;

public class RandomSentenceGenerator : IChallenge
{
    public string GetName()
    {
        return "Random Sentence Generator";
    }

    public void Play()
    {
        /**
         * https://randomwordgenerator.com/json/sentences.json
         * https://stackoverflow.com/questions/9620278/how-do-i-make-calls-to-a-rest-api-using-c
         */
        HttpClient httpClient = new HttpClient();

        HttpResponseMessage response;
        try
        {
            response = httpClient.GetAsync("https://randomwordgenerator.com/json/sentences.json").Result;
        }
        catch (AggregateException e)
        {
            if (e.InnerExceptions.Any(x => x is HttpRequestException))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Not possible to play this game: no internet connection");
                Console.ResetColor();
                (this as IChallenge).ExitChallengeText();
                return;
            }

            throw;
        }

        var responseString = response.Content.ReadAsStringAsync().Result;

        SentenceDAO sentenceDao;
        try
        {
            sentenceDao = JsonSerializer.Deserialize<SentenceDAO>(responseString)!;
        }
        catch (Exception e)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Internal error");
            Console.ResetColor();
            (this as IChallenge).ExitChallengeText();
            return;
        }

        bool playAgain = true;
        var random = new Random();
        do
        {
            (this as IChallenge).ShowNameTemplate();
            var randomNumber = random.Next(sentenceDao.data.Count);

            Console.WriteLine($"Sentence: {sentenceDao.data[randomNumber].sentence}");
            playAgain = (this as IChallenge).GetIfUserWantsToRestart();
        } while (playAgain);
    }
}