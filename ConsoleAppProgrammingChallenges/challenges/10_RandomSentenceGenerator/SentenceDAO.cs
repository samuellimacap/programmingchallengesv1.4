namespace ConsoleAppProgrammingChallenges.challenges._10_RandomSentenceGenerator;

public class SentenceDAO
{
    public List<Sentence> data { get; set; }

    public SentenceDAO(List<Sentence> data)
    {
        this.data = data;
    }
}