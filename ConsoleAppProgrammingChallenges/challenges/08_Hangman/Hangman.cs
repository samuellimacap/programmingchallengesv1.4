using System.Text;

namespace ConsoleAppProgrammingChallenges.challenges._08_Hangman;

public class Hangman : IChallenge
{
    private List<char> listTriedLetters = new List<char>();

    public HangmanGame hangmanGame;

    public Hangman()
    {
        hangmanGame = new HangmanGame();
    }

    public string GetName()
    {
        return "Hangman";
    }

    public void Play()
    {
        var wannaPlayAgain = false;
        do
        {
            StartGame();

            wannaPlayAgain = (this as IChallenge).GetIfUserWantsToRestart();
        } while (wannaPlayAgain);
    }

    private void StartGame()
    {
        hangmanGame.GenerateGame();
        do
        {
            var hangmanState = HangManStates.ReturnState(hangmanGame.totalAttempts);

            Console.WriteLine(hangmanState);
            Console.WriteLine("");

            ShowWhichLettersPlayerGotRightOfAllLetters();

            Console.WriteLine("");
            var letter = GetLetterFromUser();
            hangmanGame.GuessLetter(letter);
        } while (
            !hangmanGame.isGameLost() && !hangmanGame.isGameWon()
        );

        if (hangmanGame.isGameLost())
            ShowLoserInformation();

        if (hangmanGame.isGameWon())
            ShowWinnerInformation();
    }

    private void ShowWhichLettersPlayerGotRightOfAllLetters()
    {
        Console.Write("Word: ");
        for (int i = 0; i < hangmanGame.word.Length; i++)
            Console.Write(hangmanGame.guessedLetters[i] ? hangmanGame.word[i] + " " : "_ ");
    }

    private char GetLetterFromUser()
    {
        char letter = ' ';
        do
        {
            Console.Write("Letter: ");
            var line = Console.ReadLine();
            if (!String.IsNullOrWhiteSpace(line))
                letter = line.ToCharArray()[0];
        } while (letter == ' ');

        return letter;
    }

    private void ShowLoserInformation()
    {
        Console.WriteLine(HangManStates.ReturnState(hangmanGame.totalAttempts));
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine($"Word: {hangmanGame.word}");
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("Unfortunately you used all your attempts and not guessed the word");
        Console.Write("Word: ");
        Console.ForegroundColor = ConsoleColor.DarkCyan;
        Console.Write(hangmanGame.word);
        Console.ResetColor();
    }

    private void ShowWinnerInformation()
    {
        Console.WriteLine(HangManStates.ReturnState(hangmanGame.totalAttempts));
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine($"Word: {hangmanGame.word}");
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine("Congratulations!! You guessed all right");
        Console.ResetColor();
    }
}