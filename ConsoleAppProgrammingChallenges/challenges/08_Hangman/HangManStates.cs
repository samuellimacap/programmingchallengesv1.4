namespace ConsoleAppProgrammingChallenges.challenges._08_Hangman;

public static class HangManStates
{
    public static string Fase0 = """
                                  ------
                                  |    |
                                  |
                                  |
                                  |
                                 """;

    public static string Fase1 = """
                                  ------
                                  |    |
                                  |    0
                                  |
                                  |
                                 """;

    public static string Fase2 = """
                                  ------
                                  |    |
                                  |   _0
                                  |
                                  |
                                 """;

    public static string Fase3 = """
                                  ------
                                  |    |
                                  |   _0_
                                  |
                                  |
                                 """;

    public static string Fase4 = """
                                  ------
                                  |    |
                                  |   _0_
                                  |    |
                                  |
                                 """;

    public static string Fase5 = """
                                  ------
                                  |    |
                                  |   _0_
                                  |    |
                                  |   /
                                 """;

    public static string Fase6 = """
                                  ------
                                  |    |
                                  |   _0_
                                  |    |
                                  |   / \
                                 """;

    public static string ReturnState(int number)
    {
        switch (number)
        {
            case 0: return Fase0;
            case 1: return Fase1;
            case 2: return Fase2;
            case 3: return Fase3;
            case 4: return Fase4;
            case 5: return Fase5;
            case 6: return Fase6;
            default: return Fase0;
        }
    }
}