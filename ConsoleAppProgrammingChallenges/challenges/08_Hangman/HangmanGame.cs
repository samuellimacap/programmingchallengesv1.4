namespace ConsoleAppProgrammingChallenges.challenges._08_Hangman;

public class HangmanGame
{
    public string word { get; private set; }
    public List<bool> guessedLetters { get; private set; }
    public int totalAttempts { get; private set; }
    private static int MAX_ATTEMPTS = 6;
    public bool isPlayerGuessedAllLetters { get; private set; }

    public HangmanGame()
    {
        word = "";
        guessedLetters = new List<bool>();
        totalAttempts = 0;
        isPlayerGuessedAllLetters = false;
    }

    public void GenerateGame()
    {
        var task = ResetGame();
        task.Wait();
    }

    private async Task ResetGame()
    {
        var httpClient = new HttpClient();

        var respondeMessage = await
            httpClient.GetAsync("https://random-word-api.herokuapp.com/word?lang=en");

        respondeMessage.EnsureSuccessStatusCode();

        var wordFromResponde = await respondeMessage.Content.ReadAsStringAsync();
        var formattedWord = wordFromResponde.Remove(wordFromResponde.Length - 2)
            .Remove(0, 2);
        word = formattedWord;

        guessedLetters.Clear();
        guessedLetters.InsertRange(0, new bool[word.Length]);
        totalAttempts = 0;
        isPlayerGuessedAllLetters = false;
    }

    public bool GuessLetter(char letter)
    {
        if (word.Contains(letter))
        {
            MarkLettersAsFoundIfContains(letter);
            
            if (!guessedLetters.Contains(false))
                isPlayerGuessedAllLetters = true;
            return true;
        }

        totalAttempts++;
        return false;
    }

    private void MarkLettersAsFoundIfContains(char letter)
    {
        var position = -1;
        do
        {
            position = word.IndexOf(letter, position + 1);
            if (position != -1)
                guessedLetters[position] = true;
        } while (position != -1);
    }

    public bool isGameLost()
    {
        return totalAttempts >= MAX_ATTEMPTS;
    }

    public bool isGameWon()
    {
        return isPlayerGuessedAllLetters;
    }
}