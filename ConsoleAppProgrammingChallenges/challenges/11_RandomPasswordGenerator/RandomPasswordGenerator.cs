using System.Security.Cryptography;
using ConsoleAppProgrammingChallenges.utils;

namespace ConsoleAppProgrammingChallenges.challenges._11_RandomPasswordGenerator;

public class RandomPasswordGenerator : IChallenge
{
    public string GetName()
    {
        return "Random password Generator";
    }

    public void Play()
    {
        var playAgain = false;
        do
        {
            var length = -1;
            length = ConsoleUtils.TryRepeatedlyUntilGetANumberFromUser("Password Length:");

            if (length == -1)
            {
                (this as IChallenge).ExitChallengeText();
                return;
            }

            try
            {
                var newString = GetRandomAlphanumericCharactersString(length);

                Console.WriteLine(newString);
            }
            catch (ArgumentException e)
            {
                if (e.ParamName != null && e.ParamName.Equals("length"))
                {
                    ConsoleUtils.WriteLineErrorMessage(
                        "Length must not be an negative number or has to be lower than 2,147,483,647");
                }
            }

            playAgain = (this as IChallenge).GetIfUserWantsToRestart();
        } while (playAgain);

        (this as IChallenge).ExitChallengeText();
    }

    private string GetRandomAlphanumericCharactersString(int length)
    {
        const string alphanumericCharacters =
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
            "abcdefghijklmnopqrstuvwxyz" +
            "0123456789";

        if (length < 0)
            throw new ArgumentException("length must not be negative", "length");
        if (length > int.MaxValue / 8) // 250 million chars ought to be enough for anybody
            throw new ArgumentException("length is too big", "length");
        var characterArray = alphanumericCharacters.Distinct().ToArray();
        if (characterArray.Length == 0)
            throw new ArgumentException("characterSet must not be empty", "characterSet");

        var bytes = new byte[length * 8];
        RandomNumberGenerator.Fill(bytes);
        var result = new char[length];
        for (int i = 0; i < length; i++)
        {
            ulong value = BitConverter.ToUInt64(bytes, i * 8);
            result[i] = characterArray[value % (uint)characterArray.Length];
        }

        var newString = new string(result);
        return newString;
    }
}