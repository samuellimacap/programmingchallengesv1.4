using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using ConsoleAppProgrammingChallenges.utils;

namespace ConsoleAppProgrammingChallenges.challenges;

public class EncryptionDecryptionAlgorithmChallenge : IChallenge
{
    
    Dictionary<Option, string> optionStringValuesMap = new ()
    {
        { Option.Encrypt, "encrypt" },
        { Option.Decrypt, "decrypt" }
    };
    public string GetName()
    {
        return "Encryption/Decryption Algorithm";
    }

    public void Play()
    {
        bool tryAgain = false;
        do
        {
            var aesEncryption = Aes.Create();
            var sha256 = SHA256.Create();
            var byteArray = Encoding.UTF8.GetBytes(String.Concat(Enumerable.Repeat("1604", 4)));
            aesEncryption.IV = byteArray;

            var userOption = GetUserOption();
            if (userOption == Option.Exit)
            {
                (this as IChallenge).ExitChallengeText();
                return;
            }

            string message;
            do
            {
                Console.Write($"Type a message you want to {userOption.ToString()}:");
                message = Console.ReadLine()!;
                if (string.IsNullOrWhiteSpace(message))
                    ConsoleUtils.WriteLineErrorMessage("The message must be a no blank message");
            } while (string.IsNullOrWhiteSpace(message));


            Console.Write("Type a password to encrypt/decrypt your message:");
            var keyString = Console.ReadLine()!;
            aesEncryption.Key = sha256.ComputeHash(Encoding.UTF8.GetBytes(keyString));

            String resultMessage;
            try
            {
                if (userOption == Option.Encrypt) resultMessage = EncryptMessage(aesEncryption, message);
                else resultMessage = DecryptMessage(aesEncryption, message);


                var dictionaryMessages = new Dictionary<Option, string>()
                {
                    { Option.Encrypt, "encrypted" },
                    { Option.Decrypt, "decrypted" }
                };
                Console.WriteLine($"Message {dictionaryMessages.GetValueOrDefault(userOption, "")}: {resultMessage}");
            }
            catch (FormatException)
            {
                ConsoleUtils.WriteLineErrorMessage("The encrypted message you typed is not a valid encrypted message");
            }
            catch (CryptographicException e)
            {
                var errorMessage = new Dictionary<Option, string>()
                {
                    { Option.Encrypt, "Password is not a utf8 character or message is not a valid message"},
                    { Option.Decrypt, "The password is not a correct password to decrypt the message" }
                };
                ConsoleUtils.WriteLineErrorMessage($"Failed to {optionStringValuesMap[userOption]}. " +
                                                   errorMessage[userOption]);
            }

            tryAgain = (this as IChallenge).GetIfUserWantsToRestart();
        } while (tryAgain);
    }

    private string EncryptMessage(Aes aesEncryption, string message)
    {
        var messageUtf8Bytes = Encoding.UTF8.GetBytes(message);
        var messageEncrypted = aesEncryption.EncryptCbc(
            messageUtf8Bytes,
            aesEncryption.IV
        );

        var messageEncryptedBase64 = System.Convert.ToBase64String(messageEncrypted);
        return messageEncryptedBase64;
    }

    private string DecryptMessage(Aes aesEncryption, string messageEncryptedBase64)
    {
        var messageEncrypted = System.Convert.FromBase64String(messageEncryptedBase64);
        var messageDecrypted = aesEncryption.DecryptCbc(
            messageEncrypted, aesEncryption.IV
        );

        return Encoding.UTF8.GetString(messageDecrypted);
    }

    private Option GetUserOption()
    {
        ConsoleUtils.WriteNumeratedList(new() { "Encrypt a message", "Decrypt a message" }, "Select an option");
        bool userTypedRight;
        Option option = Option.Exit;
        do
        {
            userTypedRight = true;
            var optionsAvaliable = new int[] { -1, 1, 2 };
            var choice = ConsoleUtils.TryRepeatedlyUntilGetANumberFromUser("Option: ");
            if (!optionsAvaliable.Contains(choice))
            {
                ConsoleUtils.WriteLineErrorMessage("Please, select an option detailed above or -1 to go back");
                userTypedRight = false;
                continue;
            }

            option = choice switch
            {
                -1 => Option.Exit,
                1 => Option.Encrypt,
                _ => Option.Decrypt
            };
        } while (!userTypedRight);

        return option;
    }


    void PrintByteToArray(byte[] byteArray)
    {
        var stringBuilder = new StringBuilder();
        stringBuilder.Append('[');
        foreach (byte byteNum in byteArray)
        {
            stringBuilder.Append($"{byteNum},");
        }

        stringBuilder.Remove(stringBuilder.Length - 1, 1);
        stringBuilder.Append(']');

        Console.WriteLine(stringBuilder);
    }

    enum Option
    {
        Encrypt,
        Decrypt,
        Exit,
    }
}