using ConsoleAppProgrammingChallenges.utils;

namespace ConsoleAppProgrammingChallenges.challenges;

public class TemperatureConverterChallenge : IChallenge
{
    private readonly string?[] measures = { "Celsius", "Kelvin", "Fahrenheit" };
    private readonly char[] measuresInitialLetter = { 'C', 'K', 'F' };

    public string GetName()
    {
        return "Temperature converter";
    }

    public void Play()
    {
        (this as IChallenge).ShowNameTemplate();

        Console.WriteLine(Environment.NewLine);
        Console.WriteLine("Description: Enter a temperature with a suffix (Ex: 13 C, 28 K, 43 F).");
        Console.WriteLine("After that, choose a converter by typing the numbers 1, 2 or 3");
        string option = "Y";
        do
        {
            var temperatureMeasure = TryUntilGetTemperatureAndMeasure();
            if (temperatureMeasure == null)
            {
                option = "N";
                continue;
            }

            var measureConverterSelected = TryUntilGetMeasureToConvert(temperatureMeasure.Measure);
            if (measureConverterSelected == null)
            {
                option = "N";
                continue;
            }

            var result = ConvertTemperature(temperatureMeasure, measureConverterSelected);
            
            Console.WriteLine(
                $"Temperature converted: " +
                $"{result} {(measureConverterSelected.StartsWith("K") ? "" : "º")} {measureConverterSelected[0]}");

            var restart = (this as IChallenge).GetIfUserWantsToRestart();
            if (!restart) option = "N";
        } while (option == "Y");

        (this as IChallenge).ExitChallengeText();
    }

    private TemperatureMeasure? TryUntilGetTemperatureAndMeasure()
    {
        string[] temperatureSuffixArray;
        bool userTypedRight = true;
        double temperature = 0;
        char measureInitialLetter = 'C';
        do
        {
            if (!userTypedRight) Console.WriteLine("Let's try again");
            userTypedRight = true;
            Console.Write("Enter a temperature below: ");
            var temperatureWithSuffix = Console.ReadLine()!;
            if (temperatureWithSuffix.Trim() == "-1") return null;


            temperatureSuffixArray = temperatureWithSuffix.Trim().ToUpper().Split(' ');

            if (temperatureSuffixArray.Length != 2)
            {
                userTypedRight = false;
                ConsoleUtils.WriteLineErrorMessage("Only accepted format: Number Measure. Example: 13 C, 14 K, 23 F");
                continue;
            }

            try
            {
                var doubleNumber = double.Parse(temperatureSuffixArray[0]);
            }
            catch (Exception e)
            {
                userTypedRight = false;
                ConsoleUtils.WriteLineErrorMessage("Only accepted format: Number Measure. Example: 13 C, 14 K, 23 F");
                continue;
            }

            measureInitialLetter = temperatureSuffixArray[1].ToCharArray()[0];

            if (!measuresInitialLetter.Contains(measureInitialLetter))
            {
                userTypedRight = false;
                ConsoleUtils.WriteLineErrorMessage("Only accepted measures: C (Celsius); F (Fahrenheit); K (Kelvin);");
                continue;
            }

            bool parseSuccessful = double.TryParse(temperatureSuffixArray[0], out temperature);
            if (!parseSuccessful)
            {
                userTypedRight = false;
                ConsoleUtils.WriteLineErrorMessage("Only accepted numbers to measure temperature");
                continue;
            }
        } while (!userTypedRight);

        return new TemperatureMeasure(temperature, measureInitialLetter);
    }

    private string? TryUntilGetMeasureToConvert(char? measureToDesconsider)
    {
        Console.WriteLine("Select the temperature you want to convert: ");
        for (int i = 0; i < 3; i++)
        {
            if (measureToDesconsider != null && measures[i][0] == measureToDesconsider) continue;
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write($"{i + 1}- ");
            Console.ResetColor();
            Console.WriteLine($"{measures[i]}");
        }

        bool userTypedRight;
        int converterSelected = 0;
        do
        {
            userTypedRight = true;


            int numberTyped = ConsoleUtils.TryRepeatedlyUntilGetANumberFromUser("Select converter: ");
            if (numberTyped == -1) return null;

            converterSelected = numberTyped - 1;
            if (converterSelected < 0)
            {
                userTypedRight = false;
                ConsoleUtils.WriteLineErrorMessage("This number refers to none of measures shown above");
                continue;
            }

            if (converterSelected > measures.Length - 1)
            {
                userTypedRight = false;
                ConsoleUtils.WriteLineErrorMessage("This number refers to none of measures shown above");
                continue;
            }

            if (measuresInitialLetter[converterSelected].Equals(measureToDesconsider))
            {
                userTypedRight = false;
                ConsoleUtils.WriteLineErrorMessage("You can't select the same measure as the temperature given");
                continue;
            }
        } while (!userTypedRight);

        return measures[converterSelected];
    }

    private double ConvertTemperature(TemperatureMeasure temperatureMeasure, string measureConverterSelected)
    {
        var celsiusToOtherMeasuresMap = new Dictionary<char, Func<double, double>>
        {
            { 'F', (temperature) => (temperature * 9) / 5 + 32 },
            { 'K', (temperature) => temperature + 273.15 }
        };
        var fahrenheitToOtherMeasuresMap = new Dictionary<char, Func<double, double>>
        {
            { 'C', (temperature) => (temperature - 32) * 5 / 9 },
            { 'K', (temperature) => ((temperature - 32) * 5 / 9) + 273.15 }
        };
        var kelvinToOtherMeasuresMap = new Dictionary<char, Func<double, double>>
        {
            { 'C', (temperature) => temperature - 273.15 },
            { 'F', (temperature) => ((temperature - 273.15) * 9) / 5 + 32 }
        };
        double result = 0;
        var converterInitialLetter = measureConverterSelected[0];
        switch (temperatureMeasure.Measure)
        {
            case 'C':
            {
                result = celsiusToOtherMeasuresMap[converterInitialLetter](temperatureMeasure.Temperature);
                break;
            }
            case 'F':
            {
                result = fahrenheitToOtherMeasuresMap[converterInitialLetter](temperatureMeasure.Temperature);
                break;
            }
            case 'K':
            {
                result = kelvinToOtherMeasuresMap[converterInitialLetter](temperatureMeasure.Temperature);
                break;
            }
        }

        return Math.Round(result, 8);
    }
}

internal record TemperatureMeasure(double Temperature, char Measure);