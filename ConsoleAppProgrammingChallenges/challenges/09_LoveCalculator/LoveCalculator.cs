using System.Runtime.InteropServices.JavaScript;
using System.Text;
using Microsoft.VisualBasic.CompilerServices;

namespace ConsoleAppProgrammingChallenges.challenges._09_LoveCalculator;

public class LoveCalculator : IChallenge
{
    public string GetName()
    {
        return "Love Calculator";
    }

    public void Play()
    {
        Console.Write("Write the first name of the first person: ");
        var name1 = Console.ReadLine().ToLower();

        Console.Write("Write the first name of the second person: ");
        var name2 = Console.ReadLine().ToLower();

        var lovesOccurrenceName1 = loveOcurrenceOnName(name1);
        var lovesOccurenceName2 = loveOcurrenceOnName(name2);

        var lovesSum = Int32.Parse(lovesOccurrenceName1.Result) + Int32.Parse(lovesOccurenceName2.Result);
        var lovesSumLength = lovesSum.ToString().Length;
        var lovesSumString = lovesSum.ToString();
        if (lovesSumLength < 5)
        {
            var zeroCharRepeated = new string(Enumerable.Repeat('0', 5 - lovesSumLength).ToArray());

            lovesSumString = zeroCharRepeated + lovesSum.ToString();
        }

        var loveNumber = SumNumberNumeralsUntilGet2Numerals(lovesSumString);

        Console.WriteLine($"Love: {loveNumber}");
    }

    private Task<string> loveOcurrenceOnName(string name)
    {
        var loves = "loves";
        var lovesOcurrenceName1 = new StringBuilder("");
        foreach (var c in loves)
        {
            var countName1 = 0;
            var positionName1 = -1;
            do
            {
                positionName1 = name.IndexOf(c, positionName1 + 1);
                if (positionName1 != -1) countName1++;
            } while (positionName1 != -1);

            lovesOcurrenceName1.Append(countName1.ToString());
        }

        return Task.FromResult(lovesOcurrenceName1.ToString());
    }

    private int SumNumberNumeralsUntilGet2Numerals(string number)
    {
        string finalNumber = number;
        while (finalNumber.Length > 2)
        {
            var list = new List<int>();
            for (int i = 0; i < finalNumber.Length - 1; i++)
            {
                list.Add(
                    int.Parse(finalNumber[i].ToString())
                    + int.Parse(finalNumber[i + 1].ToString())
                );
            }

            var sb = new StringBuilder();
            list.ForEach(num => sb.Append(num.ToString()));
            finalNumber = sb.ToString();
        }

        return int.Parse(finalNumber);
    }
}