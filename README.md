# Programming Challenges

---

All task list of Programming Challenges V1.4

![Challenges](resources/readme/list.png)

## Outline
* [How to compile and run](#how-to-compile)
* [Projects/Challenges done](#projects-done)
    * [01 - Higher/Lower](#01)
    * [02 -Temperature Converte](#02)
    * [03 - Age calculator](#03)
    * [04 - Encryption/Decryption Algorithm](#04)
    * [05 - FizzBuzz](#05)
    * [06 - Rock, paper, scissors](#06)
    * [07 - Project Euler](#07)
    * [08 - Hangman](#08)
    * [09 - Love Calculator](#09)

---

---
### My Inspiration:
  https://github.com/Morasiu/ProgrammingChallenges/tree/master <br>
Note: Although I used this project to be my inspiration, all the code was made thinking by myself.

---

## <a name="how-to-compile"> How to compile and run </a>

* Open your terminal in Program.cs filepath

* Execute the command "dotnet publish -c Release"

* Run the "ConsoleAppProgrammingChallenges" file generated in "bin/Release/net7.0/publish/"
  * If you are using **linux**, the runnable file can be executed by texting in terminal "./ConsoleAppProgrammingChallenges" 
  * If you are using **windows**, just run the "ConsoleAppProgrammingChallenges.exe" file


**Challenge page Selection**

<br>![Challenge page selection](resources/readme/challengePageSelection.png)

---
## Progress 

**[09/100]**
💠🔸🔸🔸🔸🔸🔸🔸🔸🔸

🔹 -> Done

💠 -> Current

🔸-> Planning to do

---
### **<a name="projects-done">Projects done</a>**

**00** Name generator (Skipped)<br>
* Reason: I'm trying to get a whole database or text file with first and last common brazilian names;


<a name="01">**01 - Higher/Lower**</a> (Done - 03/01/2024) - C#<br>
![01](resources/readme/01_HigherLower.png)
<br>[Code!](ConsoleAppProgrammingChallenges/challenges/01_HigherLower/FindTheCorrectValueChallenge.cs)

<a name="02">**02 - Temperature Converter**</a> (Done - 04/01/2024) - C#<br>
![02](resources/readme/02_TemperatureConverter.png)
<br>[Code!](ConsoleAppProgrammingChallenges/challenges/02_TemperatureConverter/TemperatureConverterChallenge.cs)

<a name="03">**03 - Age calculator**</a> (Done 05/01/2024) - C#<br>
![03](resources/readme/03_CalculateAgeInSecons.png)
<br>[Code!](ConsoleAppProgrammingChallenges/challenges/03_CalculateAgeInSeconds/CalculateAgeInSecondsChallenge.cs)

<a name="04">**04 - Encryption/Decryption algorithm**</a> (Done 08/01/2024) - C#<br>
![04](resources/readme/04_EncryptionDecryptionAlgorithm.png)
<br>[Code!](ConsoleAppProgrammingChallenges/challenges/04_EncryptionDecryptionAlgorithm/EncryptionDecryptionAlgorithmChallenge.cs)

<a name="05">**05 - FizzBuzz**</a> (Done 13/01/2024) - C#<br>
![05](resources/readme/05_FizzBuzz.png)
<br>[Code!](ConsoleAppProgrammingChallenges/challenges/05_FizzBuzz/FizzBuzzChallenge.cs)

<a name="06">**06 - Rock, paper, scissors**</a> (Done 13/01/2024) - C#<br>
![06](resources/readme/06_RockPaperScissors.png)
<br>[Code!](ConsoleAppProgrammingChallenges/challenges/06_RockPaperScissors/RockPaperScissorsChallenge.cs)

<a name="07">**07 - Project Euler**</a> (Done) - C#<br>
![07](resources/readme/07_ProjectEuler.png)
<br>[Code!](ConsoleAppProgrammingChallenges/challenges/07_ProjectEuler/ProjectEuler.cs)

<a name="08">**08 - Hangman**</a> (Done) - C#<br>
![08](resources/readme/08_Hangman_P1.png) <br>
![08 part 2](resources/readme/08_Hangman_P2.png)
<br>[Code!](ConsoleAppProgrammingChallenges/challenges/08_Hangman/Hangman.cs)

<a name="09">**09 - Love Calculator**</a> (Done) - C#<br>
![09](resources/readme/09_LoveCalculator.png) <br>
<br>[Code!](ConsoleAppProgrammingChallenges/challenges/09_LoveCalculator/LoveCalculator.cs)
